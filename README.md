## 🍈项目介绍

基于Spring Boot框架打造，针对单体式应用进行专门设计，提供整套服务模块，努力为打造全方位企业级开发解决方案，
致力将开源版打造成超越商业版后台管理框架的项目。

- Spring Cloud版本使用Spring Cloud Alibaba技术栈
- vue2使用 [ANTD PRO VUE](https://pro.antdv.com/) 作为脚手架
- vue3使用 [Vben-Admin](https://vvbin.cn/doc-next/) 作为脚手架
- 可视化大屏使用 [Go-VIew](https://gitee.com/dromara/go-view) 进行开发
- 移动端使用 [Taro](https://taro.jd.com/) vue3+TS为技术栈。

## 基于Go-VIew的可视化大屏
> 基于 [Go-VIew](https://gitee.com/dromara/go-view) 进行开发，对应的后端服务项目地址[bootx-platform](https://gitee.com/bootx/bootx-platform)

演示地址: [地址](http://visualization.platform.bootx.cn/)

## 相对于原生GoVIew修改了哪些文件
- src/api/path/system.api.ts
- src/api/path/project.api.ts
- src/api/axios.ts
- src/api/http.ts
- src/enums/httpEnum.ts
- src/hooks/useSystemInit.hook.ts
- src/views/chart/hooks/useKeyboard.hook.ts
- src/views/chart/hooks/useSync.hook.ts
- src/views/chart/ContentConfigurations/components/CanvasPage/index.vue
- src/views/chart/ContentConfigurations/components/ChartData/components/ChartDataRequest/index.vue
- src/views/chart/ContentHeader/headerTitle/index.vue
- src/views/project/layout/components/ProjectLayoutCreate/components/CreateModal/index.vue
- src/views/project/layout/components/ProjectLayoutAsideFooter/index.vue
- src/views/project/layout/components/ProjectLayoutCreate/index.vue
- src/views/project/items/components/ProjectItemsCard/index.vue
- src/views/project/items/components/ProjectItemsModalCard/index.vue
- src/views/project/index.vue
- src/views/preview/utils/storage.ts
- src/views/login/index.vue
- src/layout/components/LayoutFooter/index.vue
- src/settings/httpSetting.ts
- types/vite-env.d.ts
- vite.config.ts
- index.html
- .env
